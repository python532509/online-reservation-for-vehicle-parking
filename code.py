import sqlite3
import tkinter as tk
from tkinter import messagebox
import re
from enum import Enum

fonts = ('Courier New', 15, 'bold')
import tkinter as tk

class Welcome:
    def _init_(self, root):
        self.root = root

        self.welcome_frame = tk.Frame(self.root, width=200, height=100, bg='hotpink')
        self.welcome_frame.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

        self.welcome_label = tk.Label(self.welcome_frame, text="Welcome to Online Reservation for Vehicle pa", font=('Helvetica', 40, 'bold'), fg='white', bg='hotpink')
        self.welcome_label.grid(row=0, column=0, padx=20, pady=20)

        self.continue_button = tk.Button(self.welcome_frame, text='Continue', bg='white', fg='steel blue', font=('Courier New', 15, 'bold'),
                                         command=self.close_welcome, cursor='hand2', activebackground='blue')
        self.continue_button.grid(row=1, column=0, padx=20, pady=20)

    def close_welcome(self):
        self.root.destroy()

if _name_ == "_main_":
    root = tk.Tk()
    root.title("Heart Bound")
    root.configure(bg="white")
    root.geometry('800x600')
    root.resizable(True, True)

    welcome = Welcome(root)
    root.mainloop()

class LoginType(Enum):
    REG = "reg"

class Login:
    def _init_(self, root, cursor):
        self.root = root
        self.child_database = []  
        self.cursor = cursor

        self.login_frame = tk.Frame(self.root, width=200, height=100, bg='hotpink')
        self.login_frame.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

        self.login_label = tk.Label(self.login_frame, text="LOGIN", font=('Helvetica', 40, 'bold'), fg='white', bg='hotpink')
        self.login_label.grid(row=0, column=1, columnspan=2)

        self.user_name = tk.Label(self.login_frame, text='NAME', font=('Helvetica', 20, 'bold'), bg='hotpink', fg='indigo', width=12)
        self.user_name.grid(row=3, column=1, pady=10)
        self.user_name_entry = tk.Entry(self.login_frame, width=15, font=fonts, bg='white')
        self.user_name_entry.grid(row=3, column=2, pady=10)

        self.user_pass = tk.Label(self.login_frame, text="PASSWORD", font=('Helvetica', 20, 'bold'), bg='hotpink', fg='indigo', width=12)
        self.user_pass.grid(row=5, column=1, pady=10)
        self.user_pass_entry = tk.Entry(self.login_frame, width=15, font=fonts, bg='white', show="*")
        self.user_pass_entry.grid(row=5, column=2, pady=10)

        self.admin_login_btn = tk.Button(self.login_frame, text='Login', bg='white', fg='steel blue', font=fonts,
                                         command=self.open_orphanage_window, cursor='hand2', activebackground='blue')
        self.admin_login_btn.grid(row=7, column=1, columnspan=2)

        self.additional_button = tk.Button(self.login_frame, text='Registration', bg='white', fg='blue', font=fonts,
                                           command=self.open_Oregistration_window, cursor='hand2', activebackground='lightblue')
        self.additional_button.grid(row=9, column=0, columnspan=4, pady=20)

        self.reg_label = tk.Label(self.login_frame, text="Don't have an account? ", bg='hotpink', font=('Helvetica', 14, 'bold'), fg='white')
        self.reg_label.grid(row=8, column=0, columnspan=4, padx=5)


    def validate_login(self, username, password, login_type):
        if login_type == LoginType.REG:
            query = 'SELECT * FROM registration WHERE username=? AND password=?'
        else:
            return False  

        self.cursor.execute(query, (username, password))
        return self.cursor.fetchone() is not None

    def open_orphanage_window(self):
        username = self.user_name_entry.get()
        password = self.user_pass_entry.get()

        if not self.validate_login(username, password, LoginType.REG):
            messagebox.showerror("Invalid Login", "Invalid username or password")
            return

        admin_window = tk.Toplevel()
        admin_window.title("Admin Menu")
        admin_window.geometry("1920x1080")

        label = tk.Label(admin_window)
        label.pack() 

        child_frame = tk.Frame(admin_window, bg="#92FBFF")
        child_frame.pack(expand=True, padx=20, pady=20)

        # Center the child_frame content using the grid geometry manager
        child_frame.grid_columnconfigure(0, weight=1)  # Make column 0 expandable
        child_frame.grid_rowconfigure(0, weight=1)     # Make row 0 expandable

        name_label = tk.Label(child_frame, text="Vehicle number", font=('Helvetica', 20, 'bold'), fg='hot pink', bg="#92FBFF")
        name_label.grid(row=1, column=6, sticky='w')

        self.name_entry = tk.Entry(child_frame, width=15, font=fonts, bg='white')
        self.name_entry.grid(row=1, column=7, sticky='w', padx=10)

        slot_label = tk.Label(child_frame, text="Parking Area", font=('Helvetica', 20, 'bold'), fg='hot pink', bg="#92FBFF")
        slot_label.grid(row=2, column=0, sticky='w')
        slot_label.grid(row=3, column=6)
        self.slot_var = tk.StringVar()
        self.slot_var.set("                Select                        ") 
        slot_options = ["In SVECW parking area", "In Dental parking area","In Temple square area"]
        slot_menu = tk.OptionMenu(child_frame, self.slot_var, *slot_options)
        slot_menu.grid(row=3, column=7)

        time_label = tk.Label(child_frame, text="Parking Time", font=('Helvetica', 20,'bold'), fg='hot pink',bg="#92FBFF")
        time_label.grid(row=4, column=6)
        self.time_var = tk.StringVar()
        self.time_var.set("                Select                        ") 
        time_options = ["7:00 AM","7:30 AM","8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", 
                 "11:00 AM", "11:30 AM", "12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", 
                 "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM"]
        time_menu = tk.OptionMenu(child_frame, self.time_var, *time_options)
        time_menu.grid(row=4, column=7)


        phone_label = tk.Label(child_frame, text="Mobile number", font=('Helvetica', 20,'bold'), fg='hot pink',bg="#92FBFF")
        phone_label.grid(row=5, column=6)
        self.phone_entry = tk.Entry(child_frame,width=15,font=fonts, bg='white')
        self.phone_entry.grid(row=5, column=7)

        
        # Add the rest of your widgets similarly

        add_child_button = tk.Button(child_frame, text="Book Slot", command=self.book_slot, bg='green', fg='white', activebackground='blue')
        add_child_button.grid(row=6, column=6, columnspan=2, pady=10)


    def book_slot(self):
      name = self.name_entry.get()
      slot = self.slot_var.get()
      time = self.time_var.get()
      phone = self.phone_entry.get()

      self.cursor.execute('''
        INSERT INTO vehicle (name, slot, time, phone)
        VALUES (?, ?, ?, ?)
       ''', (name, slot, time, phone))
      conn.commit()
      self.clear_entry_fields()
      messagebox.showinfo("Slot Booked", "Slot booked successfully!")


        
    def open_Oregistration_window(self):
        registration_window = tk.Toplevel(self.root)
        registration_window.title("Registration")
        registration_window.geometry("1920x1080")

        registration_frame = tk.Frame(registration_window, width=100, height=100)
        registration_frame.pack(expand=True, fill='both')

        reg_label = tk.Label(registration_frame, text="Registration Details", font=('Helvetica', 30, 'bold'), fg='hot pink', bg='white')
        reg_label.place(relx=0.4, rely=0.05, anchor='n')  

        details_labels = ["First name:", "Last name:", "Mobile Number:", "Email:", "Username:", "Password:"]
        details_entries = []

        for i, label_text in enumerate(details_labels):
            label = tk.Label(registration_frame, text=label_text, font=('Helvetica', 14), fg='black' , bg='white')
            label.place(relx=0.25, rely=0.15 + i * 0.1, anchor='w')  
            entry = tk.Entry(registration_frame, font=('Helvetica', 12), bg='white')
            entry.place(relx=0.55, rely=0.15 + i * 0.1, anchor='e')  
            details_entries.append(entry)
        register_button = tk.Button(registration_frame, text='Finish Registration', bg='hotpink', fg='white', font=('Helvetica', 14),
                                    command=lambda: self.validate_and_register(registration_window, details_entries), activebackground='white')


        register_button.place(relx=0.4, rely=0.78, anchor='s') 

    def validate_and_register(self, registration_window, details_entries):
        def valid_name(First_name):
            return First_name.isalpha()

        def valid_mobile_number(mobile_number):
            return mobile_number.isdigit() and len(mobile_number) == 10

        def valid_email(email):
            return re.match(r"[^@]+@[^@]+\.[^@]+", email)

        def valid_username(username):
            return username.isalnum()

        First_name, Last_name, mobile_number, email, username, password = [entry.get() for entry in details_entries]

        if not valid_name(First_name):
            self.show_error_message("Invalid first Name")
        elif not valid_name(Last_name):
            self.show_error_message("Invalid last name")
        elif not valid_mobile_number(mobile_number):
            self.show_error_message("Invalid Mobile Number")
        elif not valid_email(email):
            self.show_error_message("Invalid Email")
        elif not valid_username(username):
            self.show_error_message("Invalid Username")
        else:
            self.register_admin(registration_window, details_entries)
            self.registration_successful(registration_window)



    def show_error_message(self, message):
        tk.messagebox.showerror("Error", message)

    def registration_successful(self, registration_window):
        
        messagebox.showinfo("Registration Successful", "Registration completed successfully!")
        #registration_window.destroy()


    def register_admin(self, Oregistration_window, details_entries):
       First_name, Last_name, mobile_number, email, username, password = [entry.get() for entry in details_entries]
       cursor.execute('''
        INSERT INTO registration (First_name, Last_name, mobile_number, email, username, password)
        VALUES (?, ?, ?, ?, ?, ?)
        ''', (First_name,Last_name,mobile_number, email, username, password))
       conn.commit()
       self.registration_successful(Oregistration_window)

    def clear_entry_fields(self):
        self.name_entry.delete(0, tk.END)
        self.slot_var.set("                Select                        ")
        self.time_var.set("                Select                        ")
        self.phone_entry.delete(0, tk.END)


   

if _name_ == "_main_":
    # Connect to SQLite database
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()
    if(conn):
        print("successfully connected")
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS vehicle (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            slot INTEGER NOT NULL,
            time TEXT,
            phone TEXT
        )
    ''')
    conn.commit()
    

    cursor.execute('''
    CREATE TABLE IF NOT EXISTS registration (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        First_name TEXT NOT NULL,
        Last_name TEXT NOT NULL,
        mobile_number TEXT,
        email TEXT,
        username TEXT NOT NULL,
        password TEXT NOT NULL
       )
   ''')
    conn.commit()
    conn.commit()

    root = tk.Tk()
    root.title("Heart Bound")
    root.configure(bg="white")
    root.geometry('1920x1080')
    root.resizable(True,True)

    app = Login(root,cursor)
    root.mainloop()
    conn.close()
